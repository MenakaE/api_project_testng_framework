package TestClass_TestngFilePrameterization;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import API_Common_Methods.Common_method_handle_API;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Patch_endpoint;
import Request_Repository.Patch_Request_Repo;
import Request_Repository.Testng_Data_Provider;
import io.restassured.path.json.JsonPath;

public class Patch_TestScript extends Common_method_handle_API {

	static File log_dir;
	static String patch_endpoint;
	static String patch_requestBody;
	static String patch_responseBody;

	@BeforeTest
	public static void patch_test_setup() throws IOException {

		log_dir = Create_directory.Create_log_directory("Patch_TestScript_logs");
		patch_endpoint = Patch_endpoint.patch_endpoint_test1();
	}

	@Parameters({"Req_name","Req_job"})
	@Test()
	public static void patch_test_executor(String Req_name, String Req_job) throws IOException {

		patch_requestBody = "{\r\n"
				+ "    \"name\": \""+Req_name+"\",\r\n"
				+ "    \"job\": \""+Req_job+"\"\r\n"
				+ "}";
		
		for (int i = 0; i < 5; i++) {
			int patch_statusCode = patch_statusCode(patch_requestBody, patch_endpoint);
			System.out.println(patch_statusCode);

			if (patch_statusCode == 200) {
				patch_responseBody = patch_responseBody(patch_requestBody, patch_endpoint);
				System.out.println(patch_responseBody);

				Patch_TestScript.validator(patch_requestBody, patch_responseBody);
				break;
			} else {
				System.out.println("Expected status code for patch method (200) is not found, hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String exp_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt").substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, exp_date);
	}

	@AfterTest
	public static void patch_test_teardown() throws IOException {

		String tc_name = Patch_TestScript.class.getName();
		Handle_API_logs.evidence_creation(log_dir, tc_name, patch_endpoint, patch_requestBody,
				patch_responseBody);

	}
}
