package Request_Repository;

import org.testng.annotations.DataProvider;

public class Testng_Data_Provider {

	@DataProvider()
	public Object[][] post_requestBody(){
		return new Object[][] {
			{"morpheus","leader"},
			{"menaka","QA"},
			{"Adhi","QALead"}
		};
	}
	
	@DataProvider()
	public Object[][] put_requestBody(){
		return new Object[][] {
			{"morpheus","zion resident"},
			{"menaka","QA"},
			{"Adhi","QALead"}
		};
	}
	
	@DataProvider()
	public Object[][] patch_requestBody(){
		return new Object[][] {
			{"morpheus","zion resident"},
			{"menaka","QA"},
			{"Adhi","QALead"}
		};
	}
}
