package API_Common_Methods;

import static io.restassured.RestAssured.given;
public class Common_method_handle_API {

	//Post method
		public static int post_statusCode(String requestBody, String endpoint) {
			int statusCode = given().header("Content-Type", "application/json").body(requestBody).
					when().post(endpoint).then().extract().statusCode();
			return statusCode;
		}
		
		public static String post_responseBody(String requestBody, String endpoint) {
			String responseBody = given().header("Content-Type", "application/json").body(requestBody).
					when().post(endpoint).then().extract().response().asString();
			return responseBody;
		}
		
		//Put method
		public static int put_statusCode(String requestBody, String endpoint) {
			int statusCode = given().header("Content-Type", "application/json").body(requestBody).
					when().put(endpoint).then().extract().statusCode();
			return statusCode;
		}
		
		public static String put_responseBody(String requestBody, String endpoint) {
			String responseBody = given().header("Content-Type", "application/json").body(requestBody).
					when().put(endpoint).then().extract().response().asString();
			return responseBody;
		}
		
		//Patch method
		public static int patch_statusCode(String requestBody, String endpoint) {
			int statusCode = given().header("Content-Type", "application/json").body(requestBody).
					when().patch(endpoint).then().extract().statusCode();
			return statusCode;
		}
		
		public static String patch_responseBody(String requestBody, String endpoint) {
			String responseBody = given().header("Content-Type", "application/json").body(requestBody).
					when().patch(endpoint).then().extract().response().asString();
			return responseBody;
		}
		
		//Get method
		public static int get_statusCode(String endpoint) {
			int statusCode = given().header("Content-Type", "application/json").
					when().get(endpoint).then().extract().statusCode();
			return statusCode;
		}
		
		public static String get_responseBody(String endpoint) {
			String responseBody = given().header("Content-Type", "application/json").
					when().get(endpoint).then().extract().response().asString();
			return responseBody;
		}
		
		//Delete method
		public static int delete_statusCode(String endpoint) {
			int statusCode = given().header("Content-Type", "application/json").
					when().delete(endpoint).then().extract().statusCode();
			return statusCode;
		}

}
