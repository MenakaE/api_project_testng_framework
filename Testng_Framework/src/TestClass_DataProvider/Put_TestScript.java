package TestClass_DataProvider;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.Common_method_handle_API;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Put_endpoint;
import Request_Repository.Put_Request_Repo;
import Request_Repository.Testng_Data_Provider;
import io.restassured.path.json.JsonPath;

public class Put_TestScript extends Common_method_handle_API {

	static File log_dir;
	static String put_requestBody;
	static String put_endpoint;
	static String put_responseBody;

	@BeforeTest
	public static void put_test_setup() throws IOException {

		log_dir = Create_directory.Create_log_directory("Put_TestScript_logs");
		put_endpoint = Put_endpoint.put_endpoint_test1();
	}

	@Test(dataProvider = "put_requestBody", dataProviderClass = Testng_Data_Provider.class)
	public static void put_test_executor(String name, String job) throws IOException {

		put_requestBody = "{\r\n"
				+ "    \"name\": \""+name+"\",\r\n"
				+ "    \"job\": \""+job+"\"\r\n"
				+ "}";
		for (int i = 0; i < 5; i++) {
			int put_statusCode = put_statusCode(put_requestBody, put_endpoint);
			System.out.println(put_statusCode);

			if (put_statusCode == 200) {
				put_responseBody = put_responseBody(put_requestBody, put_endpoint);
				System.out.println(put_responseBody);

				Put_TestScript.validator(put_requestBody, put_responseBody);
				break;
			} else {
				System.out.println("Expected status code for put method (200) is not found, hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String exp_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt").substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, exp_date);
	}

	@AfterTest
	public static void put_test_teardown() throws IOException {

		String tc_name = Put_TestScript.class.getName();
		Handle_API_logs.evidence_creation(log_dir, tc_name, put_endpoint, put_requestBody, put_responseBody);

	}
}
