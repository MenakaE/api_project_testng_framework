package API_TestSuite;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.Common_method_handle_API;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Delete_endpoint;

public class Delete_TestScript extends Common_method_handle_API {
	
	static File log_dir;
	static String delete_endpoint;
	
	@BeforeTest
	public static void delete_test_setup() {
		
		log_dir = Create_directory.Create_log_directory("Delete_TestScript_logs");
		delete_endpoint = Delete_endpoint.delete_endpoint_test1();

	}
	
	@Test
	public static void delete_test_executor() throws IOException {

		for (int i = 0; i < 5; i++) {
			int delete_statusCode = delete_statusCode(delete_endpoint);
			System.out.println(delete_statusCode);

			if (delete_statusCode == 204) {
				break;
			} else {
				System.out.println("Expected status code for delete method (204) is not found, hence retrying");
			}
		}
	}
	
	@AfterTest
	public static void delete_test_teardown() throws IOException {
		
		String tc_name = Delete_TestScript.class.getName();
		Handle_API_logs.evidence_creation(log_dir, tc_name, delete_endpoint);
	
	}
}
