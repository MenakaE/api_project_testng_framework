package API_TestSuite;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.Common_method_handle_API;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Post_endpoint;
import Request_Repository.Post_Request_Repo;
import io.restassured.path.json.JsonPath;

public class Post_TestScript extends Common_method_handle_API {

	static File log_dir;
	static String post_requestBody;
	static String post_endpoint;
	static String post_responseBody;

	@BeforeTest
	public static void post_test_setup() throws IOException {

		log_dir = Create_directory.Create_log_directory("Post_TestScript_logs");

		post_requestBody = Post_Request_Repo.post_request_test1();
		post_endpoint = Post_endpoint.post_endpoint_test1();
	}

	@Test
	public static void post_test_executor() throws IOException {

		for (int i = 0; i < 5; i++) {
			int post_statusCode = post_statusCode(post_requestBody, post_endpoint);
			System.out.println(post_statusCode);

			if (post_statusCode == 201) {
				post_responseBody = post_responseBody(post_requestBody, post_endpoint);
				System.out.println(post_responseBody);

				Post_TestScript.validator(post_requestBody, post_responseBody);
				break;
			} else {
				System.out.println("Expected status code for post method (201) is not found, hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String exp_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_createdAt = jsp_res.getString("createdAt").substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, exp_date);

	}

	@AfterTest
	public static void post_test_teardown() throws IOException {
		
		String tc_name = Post_TestScript.class.getName();
		Handle_API_logs.evidence_creation(log_dir, tc_name, post_endpoint, post_requestBody,
				post_responseBody);

	}
}
