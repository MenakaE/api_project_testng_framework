package API_TestSuite;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.Common_method_handle_API;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Get_endpoint;
import io.restassured.path.json.JsonPath;

public class Get_TestScript extends Common_method_handle_API {
	
	static File log_dir;
	static String get_endpoint;
	static String get_responseBody;
	
	@BeforeTest
	public static void get_test_setup() {
		
		log_dir = Create_directory.Create_log_directory("Get_TestScript_logs");
		get_endpoint = Get_endpoint.get_endpoint_test1();

	}
	
	@Test
	public static void get_test_executor() throws IOException {

		for (int i = 0; i < 5; i++) {
			int get_statusCode = get_statusCode(get_endpoint);
			System.out.println(get_statusCode);

			if (get_statusCode == 200) {
				get_responseBody = get_responseBody(get_endpoint);
				System.out.println(get_responseBody);

				Get_TestScript.validator(get_responseBody);
				break;
				
			} else {
				System.out.println("Expected status code for get method (200) is not found, hence retrying");
			}
		}
	}

	public static void validator(String responseBody) {

		int id[] = { 7, 8, 9, 10, 11, 12 };
		String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

		JsonPath jsp_res = new JsonPath(responseBody);
		JSONObject Array_res = new JSONObject(responseBody);
		JSONArray dataArray = Array_res.getJSONArray("data");

		int count = dataArray.length();
		System.out.println(count);

		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_firstname = firstname[i];
			String exp_lastname = lastname[i];

			int res_id = dataArray.getJSONObject(i).getInt("id");
			String res_email = dataArray.getJSONObject(i).getString("email");
			String res_firstname = dataArray.getJSONObject(i).getString("first_name");
			String res_lastname = dataArray.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
		}
	}
	
	@AfterTest
	public static void get_test_teardown() throws IOException {

		String tc_name = Get_TestScript.class.getName();
		Handle_API_logs.evidence_creation(log_dir, tc_name, get_endpoint, get_responseBody);

	}
}
