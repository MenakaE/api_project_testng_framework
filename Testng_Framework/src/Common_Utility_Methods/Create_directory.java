package Common_Utility_Methods;

import java.io.File;

public class Create_directory {

	public static File Create_log_directory(String log_dir) {
		String project_Directory = System.getProperty("user.dir");
		//System.out.println("The current project directory path is : " + project_Directory);
		File directory = new File(project_Directory + "\\API_logs\\"+log_dir);

		if (directory.exists()) {
			directory.delete();
			System.out.println(directory + " is Deleted");
			directory.mkdir();
			System.out.println(directory + " is Created");
		} else {
			directory.mkdir();
			System.out.println(directory + " is Created");
		}
		return directory;
	}

}
