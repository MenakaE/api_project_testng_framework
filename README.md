# API_Project_Testng_Framework

**About Project** 

Testng is a Test Automation framework, primarily for Java based projects. This framework is created using **Testng + RestAssured** using **Java** to automate Rest API of different Http methods. Testng used for

1. Validation of test case using Assert class
2. Orchestration of test case, with annotation and xml file

My framework can be classified into 6 components

1. Test Driving Mechanism
2. Test Scripts
3. Common Functions
4. Data Driven
5. Libraries
6. Reports

**1. Test Driving Mechanism** - **Testng xml** is a test configuration file used to run and organize our test cases. 
        

- Testng supports for **Data driven testing** with two kinds of parameterization using **Testng.xml + @ Parameters** and using **@DataProvider**. 

- Testng provides for **Parallel** execution and Serial execution of test cases.

- Testng allows for **grouping of test** methods into groups like Sprint 1, Sprint 2 test cases, allows to run multiple test case in a single suite.
- **Maven surefire plugin** is added, can able to execute the test case without any IDE.

**2. TestScripts**
    

- I am having separate test case for each API, each API have different test methods. Data are provided to test methods from three ways, i.e Excel Data file, @ DataProvider, @ Parameters.

- Testng annotations, used to control the flow of methods. It is used in framework like **@ BeforeTest**, will execute before test method execution, **@ Test**, which is the test method, **@AfterTest** will be executed after test method 

- **Priority** has been given to test methods to set the test execution order. Lowest proirity will be executed first. If priority is not given, test method will be executed based on **ASCII order**.
- Validation of Test using **Assert** class from Testng

**3. Common Functions**

**API related Common Functions**
        
1. Endpoint Repository - To store all endpoints of API
2. Request Repository - Contains request payload of each API
3. Common Function - Contains functions to trigger an API and to capture status code and response body
    
**Utilities related common function**
        
1. Create Directory Utility - To create a directory for log files if it doesn't exist. If already exists, to delete and create directory
2. Create logfile Utility - Once API is executed, creates a log file and adds the corresponding endpoint, request payload, response payload and headers into a text file(log file) using Filewriter class.
3. Excel Data Extractor Utility -  To read data or variables from excel file using apache poi library 
4. Extent Listener Class - Implements ITestListener to track the events that occur during execution of API tests

**4.Data Driven**
1. **Parameterization with DataProvider** - Passes the test data to test method, which is defined in same or different class with **@DataProvider** annotation
2. **Parameterization from parameters** - Passes the test data to test method with **@ Parameters** annotation, which is defined in testng xml file.
    
3. **Excel Data File** - Consists of test data, which are reading from an excel file for request payload to test API

**5. Libraries**

Dependency management of libraries are handled by maven repository. Libraries used in my framework are
        
1. RestAssured - To trigger an API and to capture response
2. Jsonpath - To parse the response
3. Testng - For Validation of response using Assert class and for Orchestration of test cases
4. Apache poi - To read and write data from an excel file
5. Extentreport - To generate extent reports
6. Java generic library - Used for handling exceptions, to write log files

**6. Reports**

**Extent reports** - Html reports which displays the results of test case execution.

**Added Files**
    Added all my framework design related files into GitLab with the command line of GitBash with the following commands:
  
    git clone [https_link]
	git init
	git checkout -b "Testng_Checkin"
	git add *
	git status
	git commit -m "Adding testng project files into repository"
	git push origin Testng_Checkin



